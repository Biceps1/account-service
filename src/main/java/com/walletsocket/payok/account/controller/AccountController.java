package com.walletsocket.payok.account.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.walletsocket.payok.account.config.PropertiesConfig;
import com.walletsocket.payok.account.model.Account;
import com.walletsocket.payok.account.model.Transcation;
import com.walletsocket.payok.account.pojo.AccountWithdrawReqPojo;
import com.walletsocket.payok.account.pojo.CreateAccountPojo;
import com.walletsocket.payok.account.pojo.LedgerPaymentStatusPojo;
import com.walletsocket.payok.account.pojo.LedgerPaymentStatusUpdatePojo;
import com.walletsocket.payok.account.pojo.OrderCreditUpdatePojo;
import com.walletsocket.payok.account.pojo.ResponseMessage;
import com.walletsocket.payok.account.service.AccountService;
import com.walletsocket.payok.account.service.LedgerService;
import com.walletsocket.payok.account.service.OrderService;

@RestController
@CrossOrigin
public class AccountController {

	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private LedgerService ledgerService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	/**
	 * host:port/account/create
	 * 
	 * create new merchant account
	 * 
	 * @param accountID
	 * @return confirmation message
	 */
	@PostMapping(path = "/account/create")
	public @ResponseBody ResponseEntity<Object> createAccount(@RequestBody CreateAccountPojo createAccountPojo) {
		
		accountService.createAccount(createAccountPojo.getAccountID());
		LOGGER.info("Account created");
		
		return ResponseEntity.ok()
				.body(new ResponseMessage("success", "Merchant successfully signed up."));
	}
	
	
	/**
	 * host:port/account/balance?id=...
	 * 
	 * Retrieve account balance for merchant
	 * 
	 * @param accountID
	 * @return Account object
	 */
	@GetMapping(path = "/account/balance")
	public @ResponseBody ResponseEntity<Object> retrieveAccountBalance(@RequestParam("id") String accountID){
		
		Account account = accountService.retrieveAccountBalance(accountID);
		LOGGER.info("Account from DB {}", account);
			
		return ResponseEntity.ok().body(account);
	}
	
	
	/**
	 * host:port/account/withdraw
	 * 
	 * Withdraw money from account
	 * 
	 * @param accountWithdrawPojo
	 * @return ledgerPaymentStatusPojo
	 */
	@PostMapping(path = "/account/withdraw")
	public @ResponseBody ResponseEntity<Object> withdrawAccountBalance(@Valid @RequestBody AccountWithdrawReqPojo accountWithdrawReqPojo){
		
		LedgerPaymentStatusPojo ledgerPaymentStatusPojo = ledgerService.withdrawAccountBalance(accountWithdrawReqPojo);
			
		return ResponseEntity.ok().body(ledgerPaymentStatusPojo);
	}
	
	
	/**
	 * host:port/account/withdrawHistory?accountID=...
	 * 
	 * Retrieve account balance for merchant
	 * 
	 * @param accountID
	 * @return Account object
	 */
	@GetMapping(path = "/account/withdrawHistory")
	public @ResponseBody ResponseEntity<Object> retrieveAccountWithdrawHistory(@RequestParam("accountID") String accountID){
		
		List<Transcation> transcations = accountService.retrieveAccountWithdrawHistory(accountID);
		LOGGER.info("Transcation history from DB {}", transcations);
			
		return ResponseEntity.ok().body(transcations);
	}
	
	
	/**
	 * host:port/account/paymentUpdate
	 * 
	 * Update the payment status
	 * 
	 * @param upateAccountPaymentStatus object
	 * @return Confirmation message
	 */
	@PostMapping(path = "/account/paymentUpdate")
	public @ResponseBody ResponseEntity<Object> upateAccountPaymentStatus(
			@RequestBody LedgerPaymentStatusUpdatePojo ledgerPaymentStatusUpdatePojo){
		
		if(ledgerPaymentStatusUpdatePojo.getStatus().equalsIgnoreCase("success"))
		{
			LOGGER.info("Updated payment status for account, message {}", ledgerPaymentStatusUpdatePojo);
			
			ledgerService.upateAccountPaymentStatus(ledgerPaymentStatusUpdatePojo);
		}
		else 
		{
			ResponseEntity.badRequest()
			.body(new ResponseMessage("failed", "sever error from ledger service when updating payment status."));
		}	
		
		return ResponseEntity.ok()
				.body(new ResponseMessage("success", "Payment status successfully updated."));
	}
	
	
	/**
	 * host:port/account/credit
	 * 
	 * Update the credit amount for merchant account
	 * 
	 * @param OrderCreditUpdatePojo
	 * @return Confirmation message
	 */
	@PostMapping(path = "/account/credit")
	public @ResponseBody ResponseEntity<Object> upateAccountCredit(@RequestBody OrderCreditUpdatePojo orderCreditUpdatePojo){
		
		orderService.updateAccountCredit(orderCreditUpdatePojo);
		LOGGER.info("Credit request for merchant");
		
		return ResponseEntity.ok()
				.body(new ResponseMessage("success", "Account credit successfully updated."));
	}
}
