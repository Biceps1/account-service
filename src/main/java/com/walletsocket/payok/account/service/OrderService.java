package com.walletsocket.payok.account.service;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walletsocket.payok.account.config.PropertiesConfig;
import com.walletsocket.payok.account.exception.InvalidRequestException;
import com.walletsocket.payok.account.model.Account;
import com.walletsocket.payok.account.model.Balance;
import com.walletsocket.payok.account.model.Transcation;
import com.walletsocket.payok.account.pojo.OrderCreditUpdatePojo;
import com.walletsocket.payok.account.repo.AccountMongoRepository;
import com.walletsocket.payok.account.repo.TranscationMongoRepository;
import com.walletsocket.payok.account.utility.AccountUtility;

@Service
public class OrderService {

	private static final Logger LOGGER = LogManager.getLogger(OrderService.class);
	
	@Autowired
	private AccountMongoRepository accountMongoRepository;
	
	@Autowired
	private TranscationMongoRepository transcationMongoRepository;
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	
	/**
	 * Update the credit for merchant account 
	 */	
	public void updateAccountCredit(OrderCreditUpdatePojo orderCreditUpdatePojo) {
		
		if(orderCreditUpdatePojo == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_INVALIDINPUT);
		
		// Get account from the ID
		Account merchantAccount = accountMongoRepository.findByAccountID(orderCreditUpdatePojo.getAccountID());
		LOGGER.info("Merchant account from DB "+merchantAccount);
		
		Account adminAccount = accountMongoRepository.findByAccountType(propertiesConfig.getAdminAccountType());
		LOGGER.info("Admin account from DB "+adminAccount);
		
		if(merchantAccount == null || adminAccount == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_ACCOUNTDOESNOTEXIST);
		
		// Get balance with correct currency type
		Balance merhchantBalance = AccountUtility.getBalanceFromAccount(merchantAccount, orderCreditUpdatePojo.getCurrencyType());
		Balance adminBalance = AccountUtility.getBalanceFromAccount(adminAccount, orderCreditUpdatePojo.getCurrencyType());
		
		if(merhchantBalance == null || adminBalance == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOSUCHCURRENCYTYPE);
		
		// Set the pending balance for merchant account
		merhchantBalance.executeCreditTranscation(orderCreditUpdatePojo.getAmount() * ( 1-propertiesConfig.getChargeRate() ));
		// Set the pending balance for admin account
		adminBalance.executeCreditTranscation(orderCreditUpdatePojo.getAmount() * propertiesConfig.getChargeRate() );
		
		// Save new balance to database
		accountMongoRepository.save(merchantAccount);
		accountMongoRepository.save(adminAccount);
		
		// Saving the transaction to database for merchant
		Transcation merchantTranscation = new Transcation(
				orderCreditUpdatePojo.getPaymentID(),
				orderCreditUpdatePojo.getAccountID(),
				propertiesConfig.getCreditType(),
				orderCreditUpdatePojo.getAmount() * ( 1-propertiesConfig.getChargeRate()),
				orderCreditUpdatePojo.getCurrencyType(),
				"",
				new Date()
				);
		LOGGER.info("Save transcation for merchant to DB "+merchantTranscation);
		transcationMongoRepository.save(merchantTranscation);
		
		// Saving the transaction to database for admin
		Transcation adminTranscation = new Transcation(
				orderCreditUpdatePojo.getPaymentID(),
				adminAccount.getAccountID(),
				propertiesConfig.getCreditType(),
				orderCreditUpdatePojo.getAmount() * propertiesConfig.getChargeRate(),
				orderCreditUpdatePojo.getCurrencyType(),
				"",
				new Date()
				);
		LOGGER.info("Save transcation for admin to DB "+adminTranscation);
		transcationMongoRepository.save(adminTranscation);
	}

}
