package com.walletsocket.payok.account.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walletsocket.payok.account.config.PropertiesConfig;
import com.walletsocket.payok.account.exception.InvalidRequestException;
import com.walletsocket.payok.account.model.Account;
import com.walletsocket.payok.account.model.Balance;
import com.walletsocket.payok.account.model.Transcation;
import com.walletsocket.payok.account.repo.AccountMongoRepository;
import com.walletsocket.payok.account.repo.TranscationMongoRepository;
import com.walletsocket.payok.account.utility.AccountUtility;

@Service
public class AccountService {
	
	private static final Logger LOGGER = LogManager.getLogger(AccountService.class);
	
	@Autowired
	private AccountMongoRepository accountMongoRepository;
	
	@Autowired
	private TranscationMongoRepository transcationMongoRepository;
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	
	/**
	 * Create new account for merchant
	 */	
	public void createAccount(String accountID) {
		
		//Get type of coins from config
		List<String> balanceTypes = propertiesConfig.getBalanceType();
		
		List<Balance> balance = new ArrayList<>();
		
		for(String balanceType : balanceTypes) {
			balance.add(new Balance(balanceType, 0.0, 0.0));
		}
		
		Account account = new Account(accountID, propertiesConfig.getMerchantAccountType(), balance);
		
		accountMongoRepository.save(account);
	}
	
	/**
	 * Retrieve balance from merchant account
	 */	
	public Account retrieveAccountBalance(String accountID) {
		
		Account account = accountMongoRepository.findByAccountID(accountID);
		
		if(account == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_ACCOUNTDOESNOTEXIST);
		
		return account;
	}
	
	/**
	 * Withdraw balance history from merchant account 
	 */	
	public List<Transcation> retrieveAccountWithdrawHistory(String accountID) {
		
		return transcationMongoRepository.findAllByAccountID(accountID);
	}	

}
