package com.walletsocket.payok.account.service;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.walletsocket.payok.account.config.PropertiesConfig;
import com.walletsocket.payok.account.exception.FundNotSufficientException;
import com.walletsocket.payok.account.exception.InvalidRequestException;
import com.walletsocket.payok.account.model.Account;
import com.walletsocket.payok.account.model.Balance;
import com.walletsocket.payok.account.model.Transcation;
import com.walletsocket.payok.account.pojo.AccountWithdrawReqPojo;
import com.walletsocket.payok.account.pojo.LedgerPaymentStatusPojo;
import com.walletsocket.payok.account.pojo.LedgerPaymentStatusUpdatePojo;
import com.walletsocket.payok.account.repo.AccountMongoRepository;
import com.walletsocket.payok.account.repo.TranscationMongoRepository;
import com.walletsocket.payok.account.utility.AccountUtility;

@Service
public class LedgerService {
	
	private static final Logger LOGGER = LogManager.getLogger(LedgerService.class);
	
	@Autowired
	private AccountMongoRepository accountMongoRepository;
	
	@Autowired
	private TranscationMongoRepository transcationMongoRepository;
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	
	@Autowired
	private RestTemplate restTemplate;
	
	/**
	 * Withdraw balance from merchant account 
	 */
	@HystrixCommand(fallbackMethod = "ledgerStatusFallback", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")})
	public LedgerPaymentStatusPojo withdrawAccountBalance(AccountWithdrawReqPojo accountWithdrawReqPojo) {

		// Check whether account have sufficient amount to withdraw
		// Step1: Get account by ID
		Account account = accountMongoRepository.findByAccountID(accountWithdrawReqPojo.getAccountID());
		
		if(account == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_ACCOUNTDOESNOTEXIST);
		
		// Step2: Get balance type from account
		Balance balance = AccountUtility.getBalanceFromAccount(account, accountWithdrawReqPojo.getCurrencyType());
		
		if(balance == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOSUCHCURRENCYTYPE);
		
		// Step3: Compare the withdraw balance with available balance
		if(balance.getAvailableBalance() < accountWithdrawReqPojo.getAmount())
			throw new FundNotSufficientException();
		
		// Send request to ledger service
		LedgerPaymentStatusPojo ledgerPaymentStatusPojo =  this.restTemplate
				.postForObject(propertiesConfig.getLedgerService() + "/ledger/withdrawTransaction",
						accountWithdrawReqPojo, 
						LedgerPaymentStatusPojo.class);
		
		// Ensure paymentID is not empty
		if(ledgerPaymentStatusPojo.getPaymentID().isEmpty())
			throw new InvalidRequestException(AccountUtility.ERRORMSG_REQUESTFAILED);

		// Save the transaction to database
		Transcation transcation = new Transcation(
				ledgerPaymentStatusPojo.getPaymentID(),
				accountWithdrawReqPojo.getAccountID(),
				propertiesConfig.getDebitType(),
				accountWithdrawReqPojo.getAmount(),
				accountWithdrawReqPojo.getCurrencyType(),
				accountWithdrawReqPojo.getToAddrs(),
				new Date()
				);

		transcationMongoRepository.save(transcation);
		
		return ledgerPaymentStatusPojo;
	}

	private LedgerPaymentStatusPojo ledgerStatusFallback(AccountWithdrawReqPojo accountWithdrawReqPojo, Throwable e) {
		
		LOGGER.info("Thrwoable e from withdrawAccountBalance function "+ e);
		
		return new LedgerPaymentStatusPojo("failed","");
	}
	
	
	
	/**
	 * Update the payment status for merchant account 
	 */	
	public void upateAccountPaymentStatus(LedgerPaymentStatusUpdatePojo ledgerPaymentStatusUpdatePojo) {
		
		if(ledgerPaymentStatusUpdatePojo == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_INVALIDINPUT);
	
		// Update account pending balance
		// Step1: Get the transaction detail by payment ID
		Transcation transcation = transcationMongoRepository.findByPaymentID(ledgerPaymentStatusUpdatePojo.getPaymentID());
		
		if(transcation == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOTRANSCATION);
		
		// Step2: Get the account by account ID from transaction
		Account account = accountMongoRepository.findByAccountID(transcation.getAccountID());
		
		if(account == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_ACCOUNTDOESNOTEXIST);
		
		// Step3: Get balance type from account
		Balance balance = AccountUtility.getBalanceFromAccount(account, transcation.getTranscationCurrencyType());
		
		if(balance == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOSUCHCURRENCYTYPE);
		
		// Step4: Update account balance based on transaction type
		if(transcation.getPaymentType()
				.equalsIgnoreCase(propertiesConfig.getDebitType())) {
			
			balance.executeDebitTranscaion(transcation.getTransactionAmount());
		}
		else if(transcation.getPaymentType()
				.equalsIgnoreCase(propertiesConfig.getCreditType())) {
			
			balance.convertPendingToAvailable();
		}
		else 
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOTRANSCATIONTYPE);
		
		// Save the new balance to database
		accountMongoRepository.save(account);
	}
}
