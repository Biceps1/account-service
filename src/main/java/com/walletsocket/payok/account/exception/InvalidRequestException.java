package com.walletsocket.payok.account.exception;

public class InvalidRequestException extends RuntimeException {

	private static final long serialVersionUID = -6785242026815066188L;

	public InvalidRequestException(String errorMessage) {
		super(errorMessage);
	}

	public InvalidRequestException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
}