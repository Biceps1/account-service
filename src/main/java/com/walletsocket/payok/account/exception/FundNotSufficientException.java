package com.walletsocket.payok.account.exception;

public class FundNotSufficientException extends RuntimeException {

	private static final long serialVersionUID = -1030365708595681311L;

	public FundNotSufficientException() {
		super("Fund not sufficient for withdraw");
	}

	public FundNotSufficientException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
}