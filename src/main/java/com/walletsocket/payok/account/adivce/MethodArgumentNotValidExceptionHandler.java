package com.walletsocket.payok.account.adivce;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.walletsocket.payok.account.pojo.ResponseMessage;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler {

	private static final Logger LOGGER = LogManager.getLogger(AccountControllerMainExceptionHandler.class);
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody ResponseMessage methodArgumentNotValidException(MethodArgumentNotValidException e) {
    	
		String errorMessage = e.getBindingResult().getFieldErrors().get(0).getDefaultMessage();
LOGGER.error("Error in methodarugmentnotvalid advice "+errorMessage ,e);
		return new ResponseMessage("failed", errorMessage);
    }
}