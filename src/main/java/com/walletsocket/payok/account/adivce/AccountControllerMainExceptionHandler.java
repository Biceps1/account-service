package com.walletsocket.payok.account.adivce;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.walletsocket.payok.account.controller.AccountController;
import com.walletsocket.payok.account.exception.FundNotSufficientException;
import com.walletsocket.payok.account.exception.InvalidRequestException;
import com.walletsocket.payok.account.pojo.ResponseMessage;

@ControllerAdvice
public class AccountControllerMainExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LogManager.getLogger(AccountControllerMainExceptionHandler.class);
	
	@ExceptionHandler({Exception.class, IOException.class})
	public @ResponseBody ResponseEntity<ResponseMessage> handleException(final Exception e){
		LOGGER.error("controller adivce ",e);
		return ResponseEntity.badRequest()
				.body(new ResponseMessage("failed", e.getMessage()));
	}
	
	@ExceptionHandler(InvalidRequestException.class)
	public @ResponseBody ResponseEntity<ResponseMessage> handleInvalidRequestException(final InvalidRequestException e){
		LOGGER.error("controller adivce ",e);
		return ResponseEntity.badRequest()
				.body(new ResponseMessage("failed", e.getMessage()));
	}
	
	@ExceptionHandler(FundNotSufficientException.class)
	public @ResponseBody ResponseEntity<ResponseMessage> handleFundNotSufficientException(final FundNotSufficientException e){
		LOGGER.error("controller adivce ",e);
		return ResponseEntity.badRequest()
				.body(new ResponseMessage("failed", e.getMessage()));
	}
	
	@ExceptionHandler(NoSuchAlgorithmException.class)
	public @ResponseBody ResponseEntity<ResponseMessage> handleNoSuchAlgorithmException(final NoSuchAlgorithmException e){
		LOGGER.error("controller adivce ",e);
		return ResponseEntity.badRequest()
				.body(new ResponseMessage("failed", e.getMessage()));
	}
	
}