package com.walletsocket.payok.account.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("ACCOUNT_TRANSCATION_INFO")
public class Transcation {
	
	@Id
	private String _id;
	
	@Field("PAYMENT_ID")
	private String paymentID;
	
	@Field("ACCOUNT_ID")
	private String accountID;
	
	@Field("PAYMENT_TYPE")
	private String paymentType;
	
	@Field("TRANSACTION_AMOUNT")
	private double transactionAmount;
	
	@Field("TRANSACTION_CURRENCY_TYPE")
	private String transcationCurrencyType;
	
	@Field("TRANSACTION_ADDRESS")
	private String transcationAddress;
	
	@Field("CREATED_TIMESTAMP")
	private Date createdTimestamp;
	
	public Transcation() {
		super();
	}
	
	public Transcation(String paymentID, String accountID, String paymentType, double transactionAmount,
			String transcationCurrencyType, String transcationAddress, Date createdTimestamp) {
		super();
		this.paymentID = paymentID;
		this.accountID = accountID;
		this.paymentType = paymentType;
		this.transactionAmount = transactionAmount;
		this.transcationCurrencyType = transcationCurrencyType;
		this.transcationAddress = transcationAddress;
		this.createdTimestamp = createdTimestamp;
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTranscationCurrencyType() {
		return transcationCurrencyType;
	}

	public void setTranscationCurrencyType(String transcationCurrencyType) {
		this.transcationCurrencyType = transcationCurrencyType;
	}

	public String getTranscationAddress() {
		return transcationAddress;
	}

	public void setTranscationAddress(String transcationAddress) {
		this.transcationAddress = transcationAddress;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return "Transcation [paymentID=" + paymentID + ", paymentType=" + paymentType + ", transactionAmount="
				+ transactionAmount + ", transcationCurrencyType=" + transcationCurrencyType + ", transcationAddress="
				+ transcationAddress + "]";
	}

}
