package com.walletsocket.payok.account.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "ACCOUNT_INFO")
public class Account {
	
	@Id
	private String _id;
	
	@Field("ACCOUNT_ID")
	private String accountID;
	
	@Field("ACCOUNT_TYPE")
	private String accountType;
	
	@Field("ACCOUNT_BALANCE")
	private List<Balance> balance;

	public Account() {
		super();
	}

	public Account(String accountID, String accountType, List<Balance> balance) {
		super();
		this.accountID = accountID;
		this.accountType = accountType;
		this.balance = balance;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public List<Balance> getBalance() {
		return balance;
	}

	public void setBalance(List<Balance> balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [accountID=" + accountID + ", accountType=" + accountType + ", balance=" + balance + "]";
	}

}
