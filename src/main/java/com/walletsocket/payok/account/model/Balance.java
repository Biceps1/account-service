package com.walletsocket.payok.account.model;

import org.springframework.data.mongodb.core.mapping.Field;

public class Balance {
	
	@Field("CURRENCY_TYPE")
	protected String currencyType;
	
	@Field("ACCOUNT_AVAILABLEBALANCE")
	protected double availableBalance;
	
	@Field("ACCOUNT_PENDINGBALANCE")
	protected double pendingBalance;
	
	@Field("ACCOUNT_TOTALBALANCE")
	protected double totalBalance;

	public Balance() {
		super();
	}
	
	public Balance(String currencyType, double availableBalance, double pendingBalance) {
		super();
		this.currencyType = currencyType;
		this.availableBalance = availableBalance;
		this.pendingBalance = pendingBalance;
		this.totalBalance = availableBalance + pendingBalance;
	}
	
	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public double getAvailableBalance() {
		return availableBalance;
	}

	public double getPendingBalance() {
		return pendingBalance;
	}
	
	public double getTotalBalance() {
		return availableBalance + pendingBalance;
	}
	
	public void executeDebitTranscaion(double amount) {
		this.availableBalance -= amount;
		this.totalBalance -= amount;
	}
	
	public void executeCreditTranscation(double amount) {
		this.pendingBalance = amount;
		this.totalBalance += amount;
	}

	public void convertPendingToAvailable() {
		this.availableBalance += this.pendingBalance;
		this.pendingBalance = 0.0;
	}
	
	@Override
	public String toString() {
		return "Balance [currencyType=" + currencyType + ", availableBalance=" + availableBalance + ", pendingBalance="
				+ pendingBalance + ", totalBalance=" + totalBalance + "]";
	}

}
