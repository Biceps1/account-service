package com.walletsocket.payok.account.utility;

import org.springframework.beans.factory.annotation.Autowired;

import com.walletsocket.payok.account.config.PropertiesConfig;
import com.walletsocket.payok.account.exception.InvalidRequestException;
import com.walletsocket.payok.account.model.Account;
import com.walletsocket.payok.account.model.Balance;
import com.walletsocket.payok.account.repo.AccountMongoRepository;

public class AccountUtility {
	
	@Autowired
	private static AccountMongoRepository accountMongoRepository;
	
	@Autowired
	private static PropertiesConfig propertiesConfig;
	
	public static final String ERRORMSG_ACCOUNTDOESNOTEXIST = "Account doesn't exist";
	public static final String ERRORMSG_ACCOUNTTYPEDOESNOTEXIST = "Account type doesn't exist";
	public static final String ERRORMSG_NOCURRENCYAVAILABLE = "No currency available for the merchant account";
	public static final String ERRORMSG_NOSUCHCURRENCYTYPE = "Currency type doesn't exist";
	public static final String ERRORMSG_NOTRANSCATIONHISTORY = "No transcation history for this account";
	public static final String ERRORMSG_NOTRANSCATION = "No transcation fund";
	public static final String ERRORMSG_NOTRANSCATIONTYPE = "Please check transcation type";
	public static final String ERRORMSG_REQUESTFAILED = "Request failed";
	public static final String ERRORMSG_INVALIDINPUT = "Invalid input";
	
	private AccountUtility(){}
	
	public static Balance getAdminBalance(String currencyType) {
		
		Account account = accountMongoRepository.findByAccountType(propertiesConfig.getAdminAccountType());
		
		if(account == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_ACCOUNTDOESNOTEXIST);
		
		// Get balance with correct currency type
		Balance balance = getBalanceFromAccount(account, currencyType);
		
		if(balance == null)
			throw new InvalidRequestException(AccountUtility.ERRORMSG_NOSUCHCURRENCYTYPE);
		
		return balance;
	}
	
	// Function to get the Balance from the given account
	public static Balance getBalanceFromAccount(Account account, String currencyType) {
		
		Balance balance = null;
		
		for(int i=0; i<account.getBalance().size() ; i++) {
			if(account.getBalance().get(i).getCurrencyType()
					.equals(currencyType)) {
				balance = account.getBalance().get(i);
				break;
			}
		}
		
		return balance;
	}
}
