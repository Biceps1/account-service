package com.walletsocket.payok.account.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.walletsocket.payok.account.model.Account;

@Repository
public interface AccountMongoRepository extends MongoRepository<Account, String> {
	
	public Account findByAccountID(String accountID);

	public Account findByAccountType(String accountType);
}
