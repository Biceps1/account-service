package com.walletsocket.payok.account.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.walletsocket.payok.account.model.Transcation;

@Repository
public interface TranscationMongoRepository extends MongoRepository<Transcation, String> {
	
	@Query("{ PAYMENT_TYPE : 'Debit' }")
	public List<Transcation> findAllByAccountID(String accountID);
	
	public Transcation findByPaymentID(String paymentID);
	
}
