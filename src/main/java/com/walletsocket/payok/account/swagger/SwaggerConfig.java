package com.walletsocket.payok.account.swagger;

import java.util.Collections;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class SwaggerConfig will add swagger and swagger ui into the project.
 * More details at https://swagger.io/tools/swagger-ui/
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
				"ACCOUNT SERVICE",
				"API document for Account service.",
				"API TOS",
				"Terms of service",
				new Contact("Pay0k Team", "www.pay0k.com", "info@pay0k.com"),
				"License of API", "", Collections.emptyList());
	}
}

