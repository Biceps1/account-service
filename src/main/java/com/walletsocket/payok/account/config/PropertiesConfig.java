package com.walletsocket.payok.account.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "account")
public class PropertiesConfig {

	private String ledgerService = "http://ledger-service:8080";
	private List<String> balanceType = Arrays.asList("BTC","StableCoin");
	private String debitType = "Debit";
	private String creditType = "Credit";
	private double chargeRate = 0.02;
	private String adminAccountType = "Admin";
	private String merchantAccountType = "Merchant";
	
	public String getLedgerService() {
		return ledgerService;
	}
	public void setLedgerService(String ledgerService) {
		this.ledgerService = ledgerService;
	}
	public List<String> getBalanceType() {
		return balanceType;
	}
	public void setBalanceType(List<String> balanceType) {
		this.balanceType = balanceType;
	}
	public String getDebitType() {
		return debitType;
	}
	public void setDebitType(String debitType) {
		this.debitType = debitType;
	}
	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	public double getChargeRate() {
		return chargeRate;
	}
	public void setChargeRate(double chargeRate) {
		this.chargeRate = chargeRate;
	}
	public String getAdminAccountType() {
		return adminAccountType;
	}
	public void setAdminAccountType(String adminAccountType) {
		this.adminAccountType = adminAccountType;
	}
	public String getMerchantAccountType() {
		return merchantAccountType;
	}
	public void setMerchantAccountType(String merchantAccountType) {
		this.merchantAccountType = merchantAccountType;
	}

}
