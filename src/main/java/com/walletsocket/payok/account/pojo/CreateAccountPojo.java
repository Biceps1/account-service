package com.walletsocket.payok.account.pojo;

public class CreateAccountPojo {
	
	private String accountID;
	
	public CreateAccountPojo() {
		super();
	}

	public CreateAccountPojo(String accountID) {
		super();
		this.accountID = accountID;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	
}
