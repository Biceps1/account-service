package com.walletsocket.payok.account.pojo;

import javax.validation.constraints.NotEmpty;

public class OrderCreditUpdatePojo {
	
	@NotEmpty(message = "Account ID cannot be empty")
	private String accountID;
	
	@NotEmpty(message = "Payment ID cannot be empty")
	private String paymentID;
	
	private double amount;
	
	@NotEmpty(message = "Currency type cannot be empty")
	private String currencyType;
	
	public OrderCreditUpdatePojo() {
		super();
	}

	public OrderCreditUpdatePojo(String accountID, String paymentID, double amount, String currencyType) {
		super();
		this.accountID = accountID;
		this.paymentID = paymentID;
		this.amount = amount;
		this.currencyType = currencyType;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	@Override
	public String toString() {
		return "OrderCreditUpdatePojo [accountID=" + accountID + ", paymentID=" + paymentID + ", amount=" + amount
				+ ", currencyType=" + currencyType + "]";
	}
}
