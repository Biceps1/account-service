/*
 * Copyright (c)  WalletSocket Inc. 2019
 */

package com.walletsocket.payok.account.pojo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class AccountWithdrawReqPojo {

	@NotEmpty(message = "Account ID cannot be empty")
	private String accountID;
	
	@NotEmpty(message = "Withdraw address cannot be empty")
	private String toAddrs;
	
	@Min(0)
	private double amount;
	
	@NotEmpty(message = "Currency type cannot be empty")
	private String currencyType;
	
	private String callbackURL;

	public AccountWithdrawReqPojo() {
	}

	public AccountWithdrawReqPojo(String accountID, String toAddrs, double amount, String currencyType) {
		this.accountID = accountID;
		this.toAddrs = toAddrs;
		this.amount = amount;
		this.currencyType = currencyType;
	}

	public AccountWithdrawReqPojo(String accountID, String toAddrs, double amount, String currencyType, String callbackURL) {
		this.accountID = accountID;
		this.toAddrs = toAddrs;
		this.amount = amount;
		this.currencyType = currencyType;
		this.callbackURL = callbackURL;
	}

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getToAddrs() {
		return toAddrs;
	}

	public void setToAddrs(String toAddrs) {
		this.toAddrs = toAddrs;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getCallbackURL() {
		return callbackURL;
	}

	public void setCallbackURL(String callbackURL) {
		this.callbackURL = callbackURL;
	}

	@Override
	public String toString() {
		return "AccountWithdrawReqPojo [accountID=" + accountID + ", toAddrs=" + toAddrs + ", amount=" + amount
				+ ", currencyType=" + currencyType + ", callbackURL=" + callbackURL + "]";
	}

}
