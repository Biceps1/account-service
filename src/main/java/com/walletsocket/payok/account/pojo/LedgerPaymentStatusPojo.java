package com.walletsocket.payok.account.pojo;

public class LedgerPaymentStatusPojo {

	private String status;
	private String paymentID;
	
	public LedgerPaymentStatusPojo() {
	}
	
	public LedgerPaymentStatusPojo(String status, String paymentID) {
		super();
		this.status = status;
		this.paymentID = paymentID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}
}
