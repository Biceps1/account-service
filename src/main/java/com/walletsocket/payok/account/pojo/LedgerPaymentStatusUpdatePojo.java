package com.walletsocket.payok.account.pojo;

import javax.validation.constraints.NotEmpty;

public class LedgerPaymentStatusUpdatePojo {

	@NotEmpty(message = "Payment ID cannot be empty")
	private String paymentID;
	
	@NotEmpty(message = "Status cannot be empty")
	private String status;
	
	public LedgerPaymentStatusUpdatePojo() {
		super();
	}
	
	public LedgerPaymentStatusUpdatePojo(String paymentID, String status) {
		super();
		this.paymentID = paymentID;
		this.status = status;
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
