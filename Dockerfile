FROM openjdk:8-jdk-alpine
LABEL maintainer="chenyang@u.nus.edu"
VOLUME /tmp
COPY target/payok-account-service-0.1.jar app.jar
ENV JAVA_OPTS="-Xms 100m -Xmx 512m"
ENTRYPOINT exec java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9999 -jar /app.jar
